import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import Box from './boxItem/BoxItem';
import Dropdown, { OptionsType } from '../../components/dropdown/Dropdown';
import Input from '../../components/input/Input';
import Progress from '../../components/progress/Progress';
import { useAppDispatch } from '../../redux/configureStore';
import { RootState } from '../../redux/rootReducer';
import {
  addTask,
  getCountCompleteTasks,
  getTasks,
  TaskRes,
} from '../../redux/todo/actions';
import { setIsFetch } from '../../redux/utils/actions';
import { DEFAULT_DROPWOWN } from '../../utils/utils';
import { TodoBoxStyled, TodoContainerStyled } from './styles';

const TodoList = () => {
  const tasks = useSelector((state: RootState) => {
    return state.todo.getTasks;
  });

  const getTaskIsLoading = useSelector((state: RootState) => {
    return state.todo.getTaskIsLoading;
  });

  return (
    <div>
      {getTaskIsLoading ? (
        <div>
          <p>...Loading</p>
        </div>
      ) : (
        tasks.map((item: TaskRes) => {
          return (
            <div style={{ marginTop: 10, marginBottom: 10 }}>
              <Box value={item.title} completed={item.completed} data={item} />
            </div>
          );
        })
      )}
    </div>
  );
};

export default function Todo() {
  const [selected, setSelected] = useState<OptionsType>(DEFAULT_DROPWOWN[0]);
  const [value, setValue] = useState<string>('');

  const tasksTotal = useSelector((state: RootState) => {
    return state.todo.getTasks.length;
  });

  const tasksCompleteTotal = useSelector((state: RootState) => {
    return state.todo.getCountCompleteTasks;
  });

  const isfetch = useSelector((state: RootState) => {
    return state.utils.isFetch;
  });
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (isfetch) {
      dispatch(getTasks());
      dispatch(getCountCompleteTasks());
      dispatch(setIsFetch({ isFetch: false }));
    }
  }, [dispatch, isfetch]);

  useEffect(() => {
    if (selected) {
      let params = {};
      if (selected.value !== 'ALL') {
        params = { completed: !!selected.value };
      }
      dispatch(getTasks(params));
    }
  }, [dispatch, selected]);

  const addMyTask = () => {
    const params = {
      title: value,
      completed: false,
    };
    dispatch(
      addTask(params, () => {
        dispatch(setIsFetch({ isFetch: true }));
        setSelected(DEFAULT_DROPWOWN[0]);
      })
    );
  };

  const onEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.code === 'Enter') {
      addMyTask();
    }
  };

  const onDropdownChange = (option: OptionsType) => {
    setSelected(option);
    return true;
  };

  return (
    <TodoContainerStyled>
      <TodoBoxStyled>
        <div className='task-container'>
          <Progress maxlength={tasksTotal} usage={tasksCompleteTotal} />
          <div className='task-header'>
            <h2>Task</h2>
            <div>
              <Dropdown
                options={DEFAULT_DROPWOWN}
                selected={selected}
                onChange={(option) => onDropdownChange(option)}
              />
            </div>
          </div>

          <div>
            <TodoList />
          </div>
        </div>

        <div>
          <Input
            placeholder='Add your todo...'
            onKeyUp={(e) => onEnter(e)}
            value={value}
            onChange={(e) => {
              setValue(e.target.value);
            }}
          />
        </div>
      </TodoBoxStyled>
    </TodoContainerStyled>
  );
}
