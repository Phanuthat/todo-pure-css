import { colors } from '../../../utils/colors';
import styled from 'styled-components';

export const BoxStyled = styled.div`
  background-color: ${colors.white};
  border-radius: 1rem;
  border: none;
  height: 2.875rem;
  color: ${colors.dark};
  display: flex;
  flex-direction: row;
  padding: 0 1rem;
  align-items: center;
  justify-content: space-between;
  .left-group {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    gap: 14px;
  }
  .completed {
    color: ${colors.drakGray};
    text-decoration-line: line-through;
  }
`;
