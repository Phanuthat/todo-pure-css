import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { deleteTask, TaskRes, upDateTask } from '../../../redux/todo/actions';
import { setIsFetch } from '../../../redux/utils/actions';
import { DEFAULT_DROPWOWN_ACTION } from '../../../utils/utils';
import CheckBox from '../../../components/checkbox/CheckBox';
import { OptionsType } from '../../../components/dropdown/Dropdown';
import InputWithButton from '../../../components/inputWithButton/InputWithButton';
import SelectWithIcon from '../../../components/selectWithIcon/SelectWithIcon';
import { BoxStyled } from './Styles';

interface BoxType {
  value?: any;
  selectedOption?: OptionsType;
  options?: OptionsType[];
  onDropdownChange?: (option: OptionsType) => {};
  fontColor?: string;
  completed?: boolean;
  isEdit?: boolean;
  data?: TaskRes;
}

interface OnSave {
  e: React.ChangeEvent<HTMLInputElement>;
  oldData: TaskRes;
}

export default function BoxItem(props: BoxType) {
  const [selected, setSelect] = useState<string>('');
  const [checked, setChecked] = useState<boolean>(props.completed!);

  const [value, setValue] = useState<string>(props.value);
  const dispatch = useDispatch();

  const onSelectChange = (option: OptionsType) => {
    if (option.value === 'Delete') {
      dispatch(
        deleteTask({ id: props.data?.id }, () => {
          dispatch(setIsFetch({ isFetch: true }));
        })
      );
    }
    const value: string = option.value as '';
    setSelect(value);
  };

  const onClickSave = () => {
    const params = {
      ...props.data,
      title: value,
    };
    dispatch(
      upDateTask(params, () => {
        dispatch(setIsFetch({ isFetch: true }));
      })
    );

    setSelect('');

    return true;
  };

  const onClickCheck = ({ e, oldData }: OnSave) => {
    setChecked(!checked);
    const params = {
      ...props.data,
      completed: !checked,
      id: props.data?.id,
    };

    dispatch(
      upDateTask(params, () => {
        dispatch(setIsFetch({ isFetch: true }));
      })
    );
  };

  return (
    <div>
      {selected === 'Edit' ? (
        <InputWithButton
          value={value}
          btnName='save'
          onChange={(e) => setValue(e.target.value)}
          onClick={onClickSave}
        />
      ) : (
        <BoxStyled>
          <div className='left-group'>
            <CheckBox
              onChange={(e) => {
                onClickCheck({ e, oldData: props.data! });
              }}
              value={props.data?.id}
              checked={checked}
            />
            <div className={props.completed ? 'completed' : ''}>
              {' '}
              {props.value}
            </div>
          </div>

          <div>
            <SelectWithIcon
              options={DEFAULT_DROPWOWN_ACTION}
              onChange={onSelectChange}
            />
          </div>
        </BoxStyled>
      )}
    </div>
  );
}
