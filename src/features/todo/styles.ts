import { colors } from '../../utils/colors';
import styled from 'styled-components';

export const TodoContainerStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const TodoBoxStyled = styled.div`
  display: flex;
  background: ${colors.gray};
  border-radius: 20px;
  width: 720px;
  margin: 0 auto;
  padding: 4rem;
  flex-direction: column;
  .task-container {
    .task-header {
      margin-top: 24px;
      display: flex;
      align-items: center;
      justify-content: space-between;
    }
  }
`;
