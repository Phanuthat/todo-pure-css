import { combineReducers } from 'redux';
import todo from './todo/reducers';
import utils from './utils/reducers';
const appReducers = combineReducers({
  todo,
  utils,
});

export type RootState = ReturnType<typeof appReducers>;

// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}

export default appReducers;
