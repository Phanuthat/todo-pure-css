export const SET_IS_FETCH = 'SET_IS_FETCH';

interface FetchReq {
  isFetch: boolean;
}

export function setIsFetch(params: FetchReq): any {
  return (dispatch: any) => {
    dispatch({ type: SET_IS_FETCH, payload: params.isFetch });
  };
}
