import { SET_IS_FETCH } from './actions';
import { Action } from 'redux';

const initialState = {
  isFetch: true,
};

class UtilAction implements Action {
  // readonly ADD_TASK_REQUEST = ADD_TASK_REQUEST;
  // readonly ADD_TASK_SUCCESS = ADD_TASK_SUCCESS;
  // readonly ADD_TASK_FAIL = ADD_TASK_FAIL;
  type!: string;

  constructor(public payload: any) {}
}

const reducers = (state = initialState, action: UtilAction) => {
  switch (action.type) {
    case SET_IS_FETCH:
      return {
        ...state,
        isFetch: action.payload!,
      };

    default:
      return { ...state };
  }
};

export default reducers;
