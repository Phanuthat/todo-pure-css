import axios from 'axios';

interface TaskReq {
  title?: string;
  completed?: boolean;
  id?: string;
}

export interface TaskRes {
  title: string;
  completed: boolean;
  id: string;
}

export const ADD_TASK_REQUEST = 'ADD_TASK_REQUEST';
export const ADD_TASK_SUCCESS = 'ADD_TASK_SUCCESS';
export const ADD_TASK_FAIL = 'ADD_TASK_FAIL';

export function addTask(params: TaskReq, callBack?: any): any {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: ADD_TASK_REQUEST,
      });

      const res = await axios.post('/todos', params);

      dispatch({
        type: ADD_TASK_SUCCESS,
        payload: { addTask: res.data },
      });

      if (callBack) {
        callBack(res.data);
      }
    } catch (ex) {
      dispatch({
        type: ADD_TASK_FAIL,
      });
    }
  };
}

export const UPDATE_TASK_REQUEST = 'UPDATE_TASK_REQUEST';
export const UPDATE_TASK_SUCCESS = 'UPDATE_TASK_SUCCESS';
export const UPDATE_TASK_FAIL = 'UPDATE_TASK_FAIL';

export function upDateTask(params: TaskReq, callBack?: any): any {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: UPDATE_TASK_REQUEST,
      });

      const res = await axios.put(`/todos/${params.id}`, params);

      dispatch({
        type: UPDATE_TASK_SUCCESS,
        payload: { updateTask: res.data },
      });

      if (callBack) {
        callBack(res.data);
      }
    } catch (ex) {
      dispatch({
        type: UPDATE_TASK_FAIL,
      });
    }
  };
}

export const DELETE_TASK_REQUEST = 'DELETE_TASK_REQUEST';
export const DELETE_TASK_SUCCESS = 'DELETE_TASK_SUCCESS';
export const DELETE_TASK_FAIL = 'DELETE_TASK_FAIL';

export function deleteTask(params: TaskReq, callBack?: any): any {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: DELETE_TASK_REQUEST,
      });

      const res = await axios.delete(`/todos/${params.id}`);

      dispatch({
        type: DELETE_TASK_SUCCESS,
        payload: { deleteTask: res.data },
      });

      if (callBack) {
        callBack(res.data);
      }
    } catch (ex) {
      dispatch({
        type: DELETE_TASK_FAIL,
      });
    }
  };
}

export const GET_TASK_REQUEST = 'GET_TASK_REQUEST';
export const GET_TASK_SUCCESS = 'GET_TASK_SUCCESS';
export const GET_TASK_FAIL = 'GET_TASK_FAIL';

export function getTasks(params?: TaskReq): any {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: GET_TASK_REQUEST,
      });

      const res = await axios.get('/todos', { params });

      dispatch({
        type: GET_TASK_SUCCESS,
        payload: res.data,
      });
    } catch (ex) {
      dispatch({
        type: GET_TASK_FAIL,
      });
    }
  };
}

export const GET_COUNT_COMPLETE_TASK_REQUEST =
  'GET_COUNT_COMPLETE_TASK_REQUEST';
export const GET_COUNT_COMPLETE_TASK_SUCCESS =
  'GET_COUNT_COMPLETE_TASK_SUCCESS';
export const GET_COUNT_COMPLETE_TASK_FAIL = 'GET_COUNT_COMPLETE_TASK_FAIL';

export function getCountCompleteTasks(): any {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: GET_COUNT_COMPLETE_TASK_REQUEST,
      });

      const res = await axios.get('/todos', { params: { completed: true } });

      dispatch({
        type: GET_COUNT_COMPLETE_TASK_SUCCESS,
        payload: res.data?.length,
      });
    } catch (ex) {
      dispatch({
        type: GET_COUNT_COMPLETE_TASK_FAIL,
      });
    }
  };
}
