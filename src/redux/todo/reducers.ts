import { Action } from 'redux';
import {
  ADD_TASK_REQUEST,
  ADD_TASK_SUCCESS,
  ADD_TASK_FAIL,
  GET_TASK_REQUEST,
  GET_TASK_SUCCESS,
  GET_TASK_FAIL,
  UPDATE_TASK_FAIL,
  UPDATE_TASK_SUCCESS,
  UPDATE_TASK_REQUEST,
  DELETE_TASK_FAIL,
  DELETE_TASK_SUCCESS,
  DELETE_TASK_REQUEST,
  GET_COUNT_COMPLETE_TASK_FAIL,
  GET_COUNT_COMPLETE_TASK_SUCCESS,
  GET_COUNT_COMPLETE_TASK_REQUEST,
} from './actions';

const initialState = {
  addTaskIsLoading: false,
  addTaskIsFail: false,
  addTask: {},
  getTaskIsLoading: false,
  getTaskIsFail: false,
  getTasks: [],
  updateTask: {},
  updateTaskIsLoading: false,
  updateTaskIsFail: false,
  deleteTask: {},
  deleteTaskIsLoading: false,
  deleteTaskIsFail: false,
  getCountCompleteTaskTaskIsLoading: false,
  getCountCompleteTaskTaskIsFail: false,
  getCountCompleteTasks: 0,
};

class AddTaskAction implements Action {
  // readonly ADD_TASK_REQUEST = ADD_TASK_REQUEST;
  // readonly ADD_TASK_SUCCESS = ADD_TASK_SUCCESS;
  // readonly ADD_TASK_FAIL = ADD_TASK_FAIL;
  type!: string;

  constructor(public payload: any) {}
}

const reducers = (state = initialState, action: AddTaskAction) => {
  switch (action.type) {
    case ADD_TASK_REQUEST:
      return {
        ...state,
        addTaskIsLoading: true,
        addTaskIsFail: false,
      };
    case ADD_TASK_SUCCESS:
      return {
        ...state,
        addTaskIsLoading: false,
        addTaskIsFail: false,
        addTask: action.payload,
      };
    case ADD_TASK_FAIL:
      return {
        ...state,
        addTaskIsLoading: false,
        addTaskIsFail: true,
      };
    case GET_TASK_REQUEST:
      return {
        ...state,
        getTaskIsLoading: true,
        getTaskIsFail: false,
      };
    case GET_TASK_SUCCESS:
      return {
        ...state,
        getTaskIsLoading: false,
        getTaskIsFail: false,
        getTasks: action.payload,
      };
    case GET_TASK_FAIL:
      return {
        ...state,
        getTaskIsLoading: false,
        getTaskIsFail: true,
      };
    case UPDATE_TASK_REQUEST:
      return {
        ...state,
        updateTaskIsLoading: true,
        updateTaskIsFail: false,
      };
    case UPDATE_TASK_SUCCESS:
      return {
        ...state,
        updateTaskIsLoading: false,
        updateTaskIsFail: false,
        updateTasks: action.payload,
      };
    case UPDATE_TASK_FAIL:
      return {
        ...state,
        updateTaskIsLoading: false,
        updateTaskIsFail: true,
      };
    case DELETE_TASK_REQUEST:
      return {
        ...state,
        deleteTaskIsLoading: true,
        deleteTaskIsFail: false,
      };
    case DELETE_TASK_SUCCESS:
      return {
        ...state,
        deleteTaskIsLoading: false,
        deleteTaskIsFail: false,
        deleteTasks: action.payload,
      };
    case DELETE_TASK_FAIL:
      return {
        ...state,
        deleteTaskIsLoading: false,
        deleteTaskIsFail: true,
      };
    case GET_COUNT_COMPLETE_TASK_REQUEST:
      return {
        ...state,
        getCountCompleteTaskIsLoading: true,
        getCountCompleteTaskIsFail: false,
      };
    case GET_COUNT_COMPLETE_TASK_SUCCESS:
      return {
        ...state,
        getCountCompleteTaskTaskIsLoading: false,
        getCountCompleteTaskTaskIsFail: false,
        getCountCompleteTasks: action.payload,
      };
    case GET_COUNT_COMPLETE_TASK_FAIL:
      return {
        ...state,
        getCountCompleteTaskIsLoading: false,
        getCountCompleteTaskIsFail: true,
      };
    default:
      return { ...state };
  }
};

export default reducers;
