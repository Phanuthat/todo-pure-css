import { colors } from './colors';

export const DEFAULT_DROPWOWN = [
  { value: 'ALL', label: 'ALL' },
  { value: true, label: 'Done' },
  { value: false, label: 'Undone' },
];

export const DEFAULT_DROPWOWN_ACTION = [
  { value: 'Edit', label: 'Edit' },
  { value: 'Delete', label: 'Delete', color: colors.salmon },
];
