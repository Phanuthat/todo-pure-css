export const colors = {
  salmon: '#E07C7C',
  purple: '#585292',
  gray: '#F5F5F5',
  black: '#000000',
  drakGray: '#A9A9A9',
  white: '#FFFFFF',
  lightBlack: '#3B3B3B',
  lightGray: '#BCBCBC',
  dark: '#2E2E2E',
  gray90: '#E5E5E5',
  lightBrown: '#EBB9B8',
};
