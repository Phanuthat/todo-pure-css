import { colors } from '../../utils/colors';
import styled from 'styled-components';

export const InputWithButtonStyle = styled.div`
  display: flex;
  position: relative;
  height: 2.875rem;
  width: 100%;
  background-color: ${colors.white};
  border-radius: 1rem;
  padding-right: 0.5rem;

  input {
    background-color: ${colors.white};
    border-radius: 1rem;
    border: none;
    height: auto;
    width: 95%;
    padding-left: 0.5rem;
    color: ${colors.dark};
    &:focus {
      outline: none;
    }
  }
  button {
    position: absolute;
    right: 0.5em;
    top: 5px;
  }
`;
