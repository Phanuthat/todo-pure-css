import React from 'react';
import Button from '../button/Button';
import { InputWithButtonStyle } from './Styles';

interface InputWithButtonType {
  onChange?: React.ChangeEventHandler<HTMLInputElement> | undefined;
  value?: string | undefined;
  onClick?: (e?: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {};
  btnName?: string;
}

export default function InputWithButton(props: InputWithButtonType) {
  return (
    <InputWithButtonStyle>
      <input onChange={props.onChange} value={props.value} />
      <Button onClick={props.onClick} btnName={props.btnName} />
    </InputWithButtonStyle>
  );
}
