import React, { useEffect, useRef, useState } from 'react';
import { LabelColor, SelectContainerStyle } from './Styles';
import dot from '../../img/dot.svg';

interface OptionsType {
  value?: any;
  label?: string;
  color?: string;
}

interface SelectType {
  options?: OptionsType[];
  onChange: (option: OptionsType) => void;
  icon?: string;
}

export default function SelectWithIcon({
  options,
  onChange,
  icon = dot,
}: SelectType) {
  const [isActive, setIsActive] = useState<boolean>(false);
  const btnRef = useRef<HTMLImageElement>(null);
  useEffect(() => {
    const closeDropdown = (e: any) => {
      if (e?.path[0] !== btnRef.current) {
        setIsActive(false);
      }
    };
    document.body.addEventListener('click', closeDropdown);
    return () => document.body.removeEventListener('click', closeDropdown);
  }, []);

  return (
    <SelectContainerStyle>
      <div>
        <img
          className='icon'
          src={icon}
          alt='arrowDown'
          onClick={(e) => setIsActive(!isActive)}
          ref={btnRef}
        />
      </div>
      {isActive && (
        <div className='select-content'>
          {options?.map((option, index) => (
            <div
              key={index}
              onClick={() => {
                onChange(option);
                setIsActive(false);
              }}
              className='select-item'
            >
              <LabelColor fontColor={option.color}>{option.label}</LabelColor>
            </div>
          ))}
        </div>
      )}
    </SelectContainerStyle>
  );
}
