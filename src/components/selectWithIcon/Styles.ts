import { colors } from '../../utils/colors';
import styled from 'styled-components';

interface Props {
  fontColor?: string;
}

export const SelectContainerStyle = styled.div<Props>`
  width: 100%;
  user-select: none;
  position: relative;
  cursor: pointer;
  margin: 0 auto;

  .icon {
    align-items: center;
    justify-content: space-between;
    display: flex;
  }

  .select-content {
    position: absolute;
    top: 110%;
    left: 0;
    padding: 10px;
    background: ${colors.white};
    font-weight: 500;
    border-radius: 10px;
    z-index: 999;
    .select-item {
      padding: 10px;
      cursor: pointer;
      transition: all 0.2s;
      border-radius: 10px;
      &:hover {
        background: ${colors.lightGray};
        border-radius: 10px;
      }
    }
  }
`;

export const LabelColor = styled.span<Props>`
  color: ${(props) => props.fontColor || colors.black};
`;
