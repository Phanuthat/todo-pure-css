import React, { useEffect, useRef, useState } from 'react';
import { DropdownStyled } from './Styles';
import arrowDown from '../../img/arrow_down.svg';
export interface OptionsType {
  value?: string | boolean;
  label?: string;
}

interface DropdownType {
  selected?: OptionsType;
  options?: OptionsType[];
  onChange?: (option: OptionsType) => {};
}

export default function Dropdown({
  options = [],
  selected = { value: '', label: '' },
  onChange = (option: OptionsType): any => {},
}: DropdownType) {
  const [isActive, setIsActive] = useState<boolean>(false);
  const btnRef = useRef<HTMLHeadingElement>(null);
  useEffect(() => {
    const closeDropdown = (e: any) => {
      if (e?.path[0] !== btnRef.current) {
        setIsActive(false);
      }
    };
    document.body.addEventListener('click', closeDropdown);
    return () => document.body.removeEventListener('click', closeDropdown);
  }, []);

  return (
    <DropdownStyled>
      <div
        className='dropdown-btn'
        onClick={(e) => setIsActive(!isActive)}
        ref={btnRef}
      >
        {selected.label}
        <img src={arrowDown} alt='arrowDown' />
      </div>
      {isActive && (
        <div className='dropdown-content'>
          {options.map((option, index) => (
            <div
              key={index}
              onClick={(e) => {
                onChange(option);
                setIsActive(false);
              }}
              className='dropdown-item'
            >
              {option.label}
            </div>
          ))}
        </div>
      )}
    </DropdownStyled>
  );
}
