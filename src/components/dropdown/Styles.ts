import { colors } from './../../utils/colors';
import styled from 'styled-components';

export const DropdownStyled = styled.div`
  width: 100%;
  user-select: none;
  position: relative;
  min-width: 110px;
  .dropdown-btn {
    padding: 15px 20px;
    background: ${colors.white};
    box-shadow: 3px 3px 10px 10px rgba(0, 0, 0, 0.02);
    font-weight: bold;
    display: flex;
    align-items: center;
    cursor: pointer;
    justify-content: space-between;
    border-radius: 10px;
    border: none;
  }

  .dropdown-content {
    position: absolute;
    top: 110%;
    left: 0;
    padding: 10px;
    background: ${colors.white};
    font-weight: 500;
    width: 100%;
    border-radius: 10px;
    z-index: 999;
    .dropdown-item {
      padding: 10px;
      cursor: pointer;
      transition: all 0.1s;
      &:hover {
        background: ${colors.purple};
        border-radius: 10px;
      }
    }
  }
`;
