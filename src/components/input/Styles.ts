import { colors } from './../../utils/colors';
import styled from 'styled-components';

export const InputStyled = styled.input.attrs({ type: 'text' })`
  background-color: ${colors.white};
  border-radius: 1rem;
  border: none;
  height: 2.875rem;
  width: 100%;
  padding-left: 0.5rem;
  color: ${colors.dark};
  &:focus {
    outline: none;
  }
  ::placeholder {
    color: ${colors.lightGray};
    opacity: 1; /* Firefox */
  }
`;

export const InputContainer = styled.div`
  width: 100%;
  display: flex;
`;
