import React from 'react';
import { InputContainer, InputStyled } from './Styles';

interface InputType {
  onChange?: React.ChangeEventHandler<HTMLInputElement> | undefined;
  value?: string | undefined;
  placeholder?: string | undefined;
  onKeyUp?: React.KeyboardEventHandler<HTMLInputElement> | undefined;
}

export default function Input(props: InputType) {
  return (
    <InputContainer>
      <InputStyled
        placeholder={props.placeholder}
        onChange={props.onChange}
        value={props.value}
        onKeyUp={props.onKeyUp}
      />
    </InputContainer>
  );
}
