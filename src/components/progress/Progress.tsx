import React, { useEffect, useState } from 'react';
import { LinearBarStyled, ProgressStyled } from './Styles';

const LinearBar = ({ percent = 0 }) => {
  return (
    <LinearBarStyled>
      <span style={{ width: `${percent}%` }}></span>
    </LinearBarStyled>
  );
};

function Progress({ maxlength = 0, usage = 0 }) {
  const [percent, setPercent] = useState<number>(0);
  useEffect(() => {
    if (maxlength && usage) {
      const calPercent = Math.floor((usage / maxlength) * 100);
      setPercent(calPercent);
    }
  }, [maxlength, usage]);
  return (
    <ProgressStyled>
      <h3 className='progress'>Progress</h3>
      <LinearBar percent={percent}></LinearBar>
      <p className='task-complete'>{usage} completed</p>
    </ProgressStyled>
  );
}

export default Progress;
