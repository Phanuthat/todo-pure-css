import { colors } from './../../utils/colors';
import styled from 'styled-components';

export const ProgressStyled = styled.div`
  display: flex;
  padding: 0 1rem;
  flex-direction: column;
  background-color: ${colors.salmon};
  border-radius: 20px;
  .progress {
    color: ${colors.white};
  }
  .task-complete {
    color: ${colors.lightBrown};
  }
`;

export const LinearBarStyled = styled.div`
  height: 8px;
  position: relative;
  background: ${colors.lightBlack};
  border-radius: 25px;
  span {
    display: block;
    height: 100%;
    border-top-right-radius: 8px;
    border-bottom-right-radius: 8px;
    border-top-left-radius: 20px;
    border-bottom-left-radius: 20px;
    background-color: ${colors.white};

    position: relative;
    overflow: hidden;
  }
`;
