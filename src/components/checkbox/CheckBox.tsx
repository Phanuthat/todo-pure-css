import React from 'react';
import { CheckBoxStyled } from './Styles';

interface CheckBoxType {
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  value?: string | undefined;
  checked?: boolean;
}

export default function CheckBox({ onChange, value, checked }: CheckBoxType) {
  return (
    <CheckBoxStyled
      type='checkbox'
      name='check-box'
      id='default-check-box'
      onChange={(e) => {
        if (onChange) {
          onChange(e);
        }
      }}
      value={value}
      checked={checked}
    />
  );
}
