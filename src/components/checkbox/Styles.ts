import { colors } from './../../utils/colors';
import styled from 'styled-components';

export const CheckBoxStyled = styled.input.attrs({ type: 'checkbox' })`
  appearance: none;
  background-color: ${colors.white};
  margin: 0;

  font: inherit;
  width: 1.15em;
  height: 1.15em;
  border: 0.15em solid ${colors.purple};
  border-radius: 6px;
  transform: translateY(-0.075em);

  display: grid;
  place-content: center;

  &:before {
    content: '';
    width: 0.65em;
    height: 0.65em;
    transform: scale(0);
    transition: 120ms transform ease-in-out;
    box-shadow: inset 1em 1em ${colors.white};
    background-color: ${colors.purple};

    transform-origin: bottom left;
    clip-path: polygon(14% 44%, 0 65%, 50% 100%, 100% 16%, 80% 0%, 43% 62%);
  }

  &:checked::before {
    transform: scale(1);
  }

  &:checked {
    transition: background-color 120ms ease-in-out;
    background-color: ${colors.purple};
  }
`;
