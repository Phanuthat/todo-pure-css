import { colors } from './../../utils/colors';
import styled from 'styled-components';

export const ButtonStyled = styled.button`
  background-color: ${colors.purple};
  color: ${colors.white};
  border: none;
  border-radius: 1.5em;
  font-size: 0.875rem;
  padding: 10px;
  height: auto;
`;
