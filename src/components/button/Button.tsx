import React from 'react';
import { ButtonStyled } from './Styles';

export default function Button({
  onClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {},
  btnName = 'save',
}) {
  return <ButtonStyled onClick={(e) => onClick(e)}>{btnName}</ButtonStyled>;
}
